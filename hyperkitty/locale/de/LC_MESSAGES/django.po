# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Hyperkitty\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-18 17:37+0000\n"
"PO-Revision-Date: 2022-05-30 19:20+0000\n"
"Last-Translator: Ettore Atalan <atalanttore@googlemail.com>\n"
"Language-Team: German <https://hosted.weblate.org/projects/gnu-mailman/"
"hyperkitty/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.13-dev\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: forms.py:53
msgid "Add a tag..."
msgstr ""

#: forms.py:55
msgid "Add"
msgstr "Hinzufügen"

#: forms.py:56
msgid "use commas to add multiple tags"
msgstr ""

#: forms.py:64
msgid "Attach a file"
msgstr "Eine Datei anhängen"

#: forms.py:65
msgid "Attach another file"
msgstr "Eine weitere Datei anhängen"

#: forms.py:66
msgid "Remove this file"
msgstr "Diese Datei entfernen"

#: templates/hyperkitty/404.html:28
msgid "Error 404"
msgstr "Fehler 404"

#: templates/hyperkitty/404.html:30 templates/hyperkitty/500.html:31
msgid "Oh No!"
msgstr "Oh Nein!"

#: templates/hyperkitty/404.html:32
msgid "I can't find this page."
msgstr "Ich kann diese Seite nicht finden."

#: templates/hyperkitty/404.html:33 templates/hyperkitty/500.html:34
msgid "Go back home"
msgstr "Zur Startseite"

#: templates/hyperkitty/500.html:29
msgid "Error 500"
msgstr "Fehler 500"

#: templates/hyperkitty/500.html:33
msgid "Sorry, but the requested page is unavailable due to a server hiccup."
msgstr ""
"Verzeihung, aber die angeforderte Seite ist aufgrund eines Server-Fehlers "
"nicht verfügbar."

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "started"
msgstr "gestartet"

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "last active:"
msgstr "zuletzt aktiv:"

#: templates/hyperkitty/ajax/reattach_suggest.html:8
msgid "see this thread"
msgstr "diesen Thread ansehen"

#: templates/hyperkitty/ajax/reattach_suggest.html:12
msgid "(no suggestions)"
msgstr "(keine Vorschläge)"

#: templates/hyperkitty/ajax/temp_message.html:12
msgid "Sent just now, not yet distributed"
msgstr "Gerade abgesendet, noch nicht verteilt"

#: templates/hyperkitty/api.html:5
msgid "REST API"
msgstr "REST API"

#: templates/hyperkitty/api.html:7
msgid ""
"HyperKitty comes with a small REST API allowing you to programatically "
"retrieve emails and information."
msgstr ""
"HyperKitty bietet eine kleine REST API an, welche es erlaubt, E-Mails und "
"Informationen via Code abzurufen."

#: templates/hyperkitty/api.html:10
msgid "Formats"
msgstr "Formate"

#: templates/hyperkitty/api.html:12
msgid ""
"This REST API can return the information into several formats.  The default "
"format is html to allow human readibility."
msgstr ""
"Die REST-API kann die Informationen in mehreren Formaten zurück liefern. Die "
"Standardeinstellung ist das für Menschen lesbare HTML Format."

#: templates/hyperkitty/api.html:14
msgid ""
"To change the format, just add <em>?format=&lt;FORMAT&gt;</em> to the URL."
msgstr ""
"Um dieses Format zu ändern, einfach <em>?format=&lt;FORMAT&gt;</em> an die "
"URL anfügen."

#: templates/hyperkitty/api.html:16
msgid "The list of available formats is:"
msgstr "Die Liste der verfügbaren Formate ist:"

#: templates/hyperkitty/api.html:20
msgid "Plain text"
msgstr "Plain-Text"

#: templates/hyperkitty/api.html:26
msgid "List of mailing-lists"
msgstr "Liste der Mailing-Listen"

#: templates/hyperkitty/api.html:27 templates/hyperkitty/api.html:33
#: templates/hyperkitty/api.html:39 templates/hyperkitty/api.html:45
#: templates/hyperkitty/api.html:51
msgid "Endpoint:"
msgstr "Endpunkt:"

#: templates/hyperkitty/api.html:29
msgid ""
"Using this address you will be able to retrieve the information known about "
"all the mailing lists."
msgstr ""
"Wenn diese Adresse benutzt wird, dann erhält man Informationen über alle "
"Mailing-Listen."

#: templates/hyperkitty/api.html:32
msgid "Threads in a mailing list"
msgstr "Diskussionsverläufe in Mailing-Listen"

#: templates/hyperkitty/api.html:35
msgid ""
"Using this address you will be able to retrieve information about all the "
"threads on the specified mailing list."
msgstr ""
"Wenn diese Adresse benutzt wird, dann erhält man Informationen über alle "
"Diskussionsverläufe auf den ausgewählten Mailing-Listen."

#: templates/hyperkitty/api.html:38
msgid "Emails in a thread"
msgstr "E-Mails in einem Diskussionsstrang"

#: templates/hyperkitty/api.html:41
msgid ""
"Using this address you will be able to retrieve the list of emails in a "
"mailing list thread."
msgstr ""
"Wenn diese Adresse benutzt wird, dann erhält man eine Liste aller E-Mails "
"innerhalb eines Mailing-Listen Diskussionsstrangs."

#: templates/hyperkitty/api.html:44
msgid "An email in a mailing list"
msgstr "Eine E-Mail in einer Mailing-Liste"

#: templates/hyperkitty/api.html:47
msgid ""
"Using this address you will be able to retrieve the information known about "
"a specific email on the specified mailing list."
msgstr ""
"Wenn diese Adresse benutzt wird, dann erhält man alle verfügbaren "
"Informationen über eine spezifische E-Mail auf der ausgewählten Mailing-"
"Liste."

#: templates/hyperkitty/api.html:50
msgid "Tags"
msgstr "Stichworte"

#: templates/hyperkitty/api.html:53
msgid "Using this address you will be able to retrieve the list of tags."
msgstr "Wenn diese Adresse benutzt wird, dann erhält man die Stichwortliste."

#: templates/hyperkitty/base.html:57 templates/hyperkitty/base.html:112
msgid "Account"
msgstr "Konto"

#: templates/hyperkitty/base.html:62 templates/hyperkitty/base.html:117
msgid "Mailman settings"
msgstr "Mailman-Einstellungen"

#: templates/hyperkitty/base.html:67 templates/hyperkitty/base.html:122
#: templates/hyperkitty/user_profile/base.html:17
msgid "Posting activity"
msgstr "Posting-Aktivitäten"

#: templates/hyperkitty/base.html:72 templates/hyperkitty/base.html:127
msgid "Logout"
msgstr "Ausloggen"

#: templates/hyperkitty/base.html:78 templates/hyperkitty/base.html:134
msgid "Sign In"
msgstr "Anmelden"

#: templates/hyperkitty/base.html:82 templates/hyperkitty/base.html:138
msgid "Sign Up"
msgstr "Registrieren"

#: templates/hyperkitty/base.html:91
msgid "Search this list"
msgstr "Diese Liste durchsuchen"

#: templates/hyperkitty/base.html:91
msgid "Search all lists"
msgstr "Alle Listen durchsuchen"

#: templates/hyperkitty/base.html:149
msgid "Manage this list"
msgstr "Diese Liste verwalten"

#: templates/hyperkitty/base.html:154
msgid "Manage lists"
msgstr "Listen verwalten"

#: templates/hyperkitty/base.html:192
msgid "Keyboard Shortcuts"
msgstr "Tastenkürzel"

#: templates/hyperkitty/base.html:195
msgid "Thread View"
msgstr "Thread-Ansicht"

#: templates/hyperkitty/base.html:197
msgid "Next unread message"
msgstr "Nächste ungelesene Nachricht"

#: templates/hyperkitty/base.html:198
msgid "Previous unread message"
msgstr "Vorherige ungelesene Nachricht"

#: templates/hyperkitty/base.html:199
msgid "Jump to all threads"
msgstr "Zu allen Threads wechseln"

#: templates/hyperkitty/base.html:200
msgid "Jump to MailingList overview"
msgstr "Zur Listenübersicht springen"

#: templates/hyperkitty/base.html:214
msgid "Powered by"
msgstr "Angetrieben von"

#: templates/hyperkitty/base.html:214
msgid "version"
msgstr "Version"

#: templates/hyperkitty/errors/notimplemented.html:7
msgid "Not implemented yet"
msgstr "Noch nicht implementiert"

#: templates/hyperkitty/errors/notimplemented.html:12
msgid "Not implemented"
msgstr "Nicht implementiert"

#: templates/hyperkitty/errors/notimplemented.html:14
msgid "This feature has not been implemented yet, sorry."
msgstr "Verzeihung, diese Funktionalität wurde noch nicht implementiert."

#: templates/hyperkitty/errors/private.html:7
msgid "Error: private list"
msgstr "Fehler: private Liste"

#: templates/hyperkitty/errors/private.html:19
msgid ""
"This mailing list is private. You must be subscribed to view the archives."
msgstr ""
"Diese Mailing-Liste ist privat. Sie müssen auf der Liste eingeschrieben "
"sein, um das Archiv einsehen zu dürfen."

#: templates/hyperkitty/fragments/like_form.html:16
msgid "You like it (cancel)"
msgstr "Ihnen gefällt es (abbrechen)"

#: templates/hyperkitty/fragments/like_form.html:24
msgid "You dislike it (cancel)"
msgstr "Ihnen gefällt es nicht (abbrechen)"

#: templates/hyperkitty/fragments/like_form.html:27
#: templates/hyperkitty/fragments/like_form.html:31
msgid "You must be logged-in to vote."
msgstr "Sie müssen sich anmelden, um abstimmen zu dürfen."

#: templates/hyperkitty/fragments/month_list.html:6
msgid "Threads by"
msgstr "Thread nach"

#: templates/hyperkitty/fragments/month_list.html:6
msgid " month"
msgstr " Monat"

#: templates/hyperkitty/fragments/overview_threads.html:11
msgid "New messages in this thread"
msgstr "Neue Nachrichten in diesem Diskussionsstrang"

#: templates/hyperkitty/fragments/overview_threads.html:36
#: templates/hyperkitty/fragments/thread_left_nav.html:19
#: templates/hyperkitty/overview.html:78
msgid "All Threads"
msgstr "Alle Threads"

#: templates/hyperkitty/fragments/overview_top_posters.html:15
msgid "See the profile"
msgstr "Profil anzeigen"

#: templates/hyperkitty/fragments/overview_top_posters.html:21
msgid "posts"
msgstr "Postings"

#: templates/hyperkitty/fragments/overview_top_posters.html:26
msgid "No posters this month (yet)."
msgstr "(Noch) keine Verfasser in diesem Monat."

#: templates/hyperkitty/fragments/send_as.html:5
msgid "This message will be sent as:"
msgstr "Nachricht wird gesendet als:"

#: templates/hyperkitty/fragments/send_as.html:6
msgid "Change sender"
msgstr "Absender ändern"

#: templates/hyperkitty/fragments/send_as.html:16
msgid "Link another address"
msgstr "Eine andere Adresse verknüpfen"

#: templates/hyperkitty/fragments/send_as.html:20
msgid ""
"If you aren't a current list member, sending this message will subscribe you."
msgstr ""
"Wenn Sie kein Mitglied der Liste sind, werden Sie durch das Absenden dieser "
"Nachricht eingetragen."

#: templates/hyperkitty/fragments/thread_left_nav.html:12
msgid "List overview"
msgstr "Listenübersicht"

#: templates/hyperkitty/fragments/thread_left_nav.html:29 views/message.py:74
#: views/mlist.py:114 views/thread.py:191
msgid "Download"
msgstr "Herunterladen"

#: templates/hyperkitty/fragments/thread_left_nav.html:32
msgid "Past 30 days"
msgstr "Die letzten 30 Tage"

#: templates/hyperkitty/fragments/thread_left_nav.html:33
msgid "This month"
msgstr "Diesen Monat"

#: templates/hyperkitty/fragments/thread_left_nav.html:36
msgid "Entire archive"
msgstr "Ganzes Archiv"

#: templates/hyperkitty/index.html:9 templates/hyperkitty/index.html:63
msgid "Available lists"
msgstr "Verfügbare Listen"

#: templates/hyperkitty/index.html:22 templates/hyperkitty/index.html:27
#: templates/hyperkitty/index.html:72
msgid "Most popular"
msgstr "Beliebteste"

#: templates/hyperkitty/index.html:26
msgid "Sort by number of recent participants"
msgstr "Nach Anzahl von letzten Teilnehmern sortieren"

#: templates/hyperkitty/index.html:32 templates/hyperkitty/index.html:37
#: templates/hyperkitty/index.html:75
msgid "Most active"
msgstr "Aktivste"

#: templates/hyperkitty/index.html:36
msgid "Sort by number of recent discussions"
msgstr "Nach Anzahl von letzten Diskussionen sortieren"

#: templates/hyperkitty/index.html:42 templates/hyperkitty/index.html:47
#: templates/hyperkitty/index.html:78
msgid "By name"
msgstr "Nach Name"

#: templates/hyperkitty/index.html:46
msgid "Sort alphabetically"
msgstr "Alphabetisch sortieren"

#: templates/hyperkitty/index.html:52 templates/hyperkitty/index.html:57
#: templates/hyperkitty/index.html:81
msgid "Newest"
msgstr "Neuste"

#: templates/hyperkitty/index.html:56
msgid "Sort by list creation date"
msgstr "Nach Listenerstelldatum sortieren"

#: templates/hyperkitty/index.html:68
msgid "Sort by"
msgstr "Sortieren nach"

#: templates/hyperkitty/index.html:91
msgid "Hide inactive"
msgstr "Inaktive verstecken"

#: templates/hyperkitty/index.html:92
msgid "Hide private"
msgstr "Private verstecken"

#: templates/hyperkitty/index.html:99
msgid "Find list"
msgstr "Liste finden"

#: templates/hyperkitty/index.html:123 templates/hyperkitty/index.html:191
#: templates/hyperkitty/user_profile/last_views.html:34
#: templates/hyperkitty/user_profile/last_views.html:73
msgid "new"
msgstr "neu"

#: templates/hyperkitty/index.html:135 templates/hyperkitty/index.html:202
msgid "private"
msgstr "privat"

#: templates/hyperkitty/index.html:137 templates/hyperkitty/index.html:204
msgid "inactive"
msgstr "inaktiv"

#: templates/hyperkitty/index.html:143 templates/hyperkitty/index.html:229
#: templates/hyperkitty/overview.html:94 templates/hyperkitty/overview.html:111
#: templates/hyperkitty/overview.html:181
#: templates/hyperkitty/overview.html:188
#: templates/hyperkitty/overview.html:195
#: templates/hyperkitty/overview.html:204
#: templates/hyperkitty/overview.html:212 templates/hyperkitty/reattach.html:39
#: templates/hyperkitty/thread.html:111
msgid "Loading..."
msgstr "Laden..."

#: templates/hyperkitty/index.html:160 templates/hyperkitty/index.html:237
msgid "No archived list yet."
msgstr "Noch keine archivierten Listen."

#: templates/hyperkitty/index.html:172
#: templates/hyperkitty/user_profile/favorites.html:40
#: templates/hyperkitty/user_profile/last_views.html:45
#: templates/hyperkitty/user_profile/profile.html:15
#: templates/hyperkitty/user_profile/subscriptions.html:41
#: templates/hyperkitty/user_profile/votes.html:46
msgid "List"
msgstr "Liste"

#: templates/hyperkitty/index.html:173
msgid "Description"
msgstr "Beschreibung"

#: templates/hyperkitty/index.html:174
msgid "Activity in the past 30 days"
msgstr "Aktivität in den letzten 30 Tagen"

#: templates/hyperkitty/index.html:218 templates/hyperkitty/overview.html:103
#: templates/hyperkitty/thread_list.html:50
#: templates/hyperkitty/threads/right_col.html:97
#: templates/hyperkitty/threads/summary_thread_large.html:54
msgid "participants"
msgstr "Teilnehmer"

#: templates/hyperkitty/index.html:223 templates/hyperkitty/overview.html:104
#: templates/hyperkitty/thread_list.html:55
msgid "discussions"
msgstr "Diskussionen"

#: templates/hyperkitty/list_delete.html:7
msgid "Delete MailingList"
msgstr "Liste löschen"

#: templates/hyperkitty/list_delete.html:20
#, fuzzy
#| msgid "Delete Mailing List"
msgid "Delete Mailing List From HyperKitty"
msgstr "Liste löschen"

#: templates/hyperkitty/list_delete.html:26
#, fuzzy
#| msgid ""
#| "will be deleted along with all the threads and messages. Do you want to "
#| "continue?"
msgid ""
"will be deleted from HyperKitty along with all the threads and messages. It "
"will not be deleted from Mailman Core. Do you want to continue?"
msgstr ""
"wird mit allen Threads und Nachrichten gelöscht. Möchten Sie fortfahren?"

#: templates/hyperkitty/list_delete.html:33
#: templates/hyperkitty/message_delete.html:44
msgid "Delete"
msgstr "Löschen"

#: templates/hyperkitty/list_delete.html:34
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
msgid "or"
msgstr "oder"

#: templates/hyperkitty/list_delete.html:36
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
#: templates/hyperkitty/user_profile/votes.html:36
#: templates/hyperkitty/user_profile/votes.html:74
msgid "cancel"
msgstr "abbrechen"

#: templates/hyperkitty/message.html:22
msgid "thread"
msgstr "Diskussionsstrang"

#: templates/hyperkitty/message_delete.html:7
#: templates/hyperkitty/message_delete.html:20
msgid "Delete message(s)"
msgstr "Nachricht(en) löschen"

#: templates/hyperkitty/message_delete.html:25
#, python-format
msgid ""
"\n"
"        %(count)s message(s) will be deleted. Do you want to continue?\n"
"        "
msgstr ""
"\n"
"        %(count)s Nachricht(en) werden gelöscht. Fortfahren?\n"
"        "

#: templates/hyperkitty/message_new.html:8
#: templates/hyperkitty/message_new.html:21
msgid "Create a new thread"
msgstr "Neuen Diskussionsstrang erstellen"

#: templates/hyperkitty/message_new.html:22
#: templates/hyperkitty/user_posts.html:22
msgid "in"
msgstr "in"

#: templates/hyperkitty/message_new.html:52
#: templates/hyperkitty/messages/message.html:145
msgid "Send"
msgstr "Senden"

#: templates/hyperkitty/messages/message.html:18
#, python-format
msgid "See the profile for %(name)s"
msgstr "Das Profil für %(name)s sehen"

#: templates/hyperkitty/messages/message.html:28
msgid "Unread"
msgstr "Nicht gelesen"

#: templates/hyperkitty/messages/message.html:45
msgid "Sender's time:"
msgstr "Absendezeit:"

#: templates/hyperkitty/messages/message.html:51
msgid "New subject:"
msgstr "Neuer Betreff:"

#: templates/hyperkitty/messages/message.html:61
msgid "Attachments:"
msgstr "Anhänge:"

#: templates/hyperkitty/messages/message.html:76
msgid "Display in fixed font"
msgstr "In fixierter Schrift anzeigen"

#: templates/hyperkitty/messages/message.html:79
msgid "Permalink for this message"
msgstr "Permalink für diese Nachricht"

#: templates/hyperkitty/messages/message.html:90
#: templates/hyperkitty/messages/message.html:96
msgid "Reply"
msgstr "Antworten"

#: templates/hyperkitty/messages/message.html:93
msgid "Sign in to reply online"
msgstr "Anmelden, um online zu antworten"

#: templates/hyperkitty/messages/message.html:105
#, python-format
msgid ""
"\n"
"                %(email.attachments_count)s attachment\n"
"                "
msgid_plural ""
"\n"
"                %(email.attachments_count)s attachments\n"
"                "
msgstr[0] ""
"\n"
"                %(email.attachments_count)s Anhang\n"
"                "
msgstr[1] ""
"\n"
"                %(email.attachments_count)s Anhänge\n"
"                "

#: templates/hyperkitty/messages/message.html:131
msgid "Quote"
msgstr "Zitat"

#: templates/hyperkitty/messages/message.html:132
msgid "Create new thread"
msgstr "Neuen Diskussionsstrang erstellen"

#: templates/hyperkitty/messages/message.html:135
msgid "Use email software"
msgstr "E-Mail Software benutzen"

#: templates/hyperkitty/messages/right_col.html:11
msgid "Back to the thread"
msgstr "Zurück zum Diskussionsstrang"

#: templates/hyperkitty/messages/right_col.html:18
msgid "Back to the list"
msgstr "Zurück zur Liste"

#: templates/hyperkitty/messages/right_col.html:27
msgid "Delete this message"
msgstr "Diese Nachricht löschen"

#: templates/hyperkitty/messages/summary_message.html:23
#, python-format
msgid ""
"\n"
"                                by %(name)s\n"
"                            "
msgstr ""
"\n"
"                                von %(name)s\n"
"                            "

#: templates/hyperkitty/overview.html:38
msgid "Home"
msgstr "Startseite"

#: templates/hyperkitty/overview.html:41 templates/hyperkitty/thread.html:78
msgid "Stats"
msgstr "Statistiken"

#: templates/hyperkitty/overview.html:44
msgid "Threads"
msgstr "Threads"

#: templates/hyperkitty/overview.html:50 templates/hyperkitty/overview.html:61
#: templates/hyperkitty/thread_list.html:58
msgid "You must be logged-in to create a thread."
msgstr "Sie müssen angemeldet sein, um einen Diskussionsstrang zu erstellen."

#: templates/hyperkitty/overview.html:50
msgid "New"
msgstr "Neu"

#: templates/hyperkitty/overview.html:63
#: templates/hyperkitty/thread_list.html:62
msgid ""
"<span class=\"d-none d-md-inline\">Start a n</span><span class=\"d-md-none"
"\">N</span>ew thread"
msgstr ""
"<span class=\"d-none d-md-inline\">Einen n</span><span class=\"d-md-none\">N</"
"span>euen Thread beginnen"

#: templates/hyperkitty/overview.html:75
msgid ""
"<span class=\"d-none d-md-inline\">Manage s</span><span class=\"d-md-none"
"\">S</span>ubscription"
msgstr ""
"Mitgliedschaft<span class=\"d-none d-md-inline\"> verwalten</span>"

#: templates/hyperkitty/overview.html:81
msgid "Delete Archive"
msgstr "Archiv löschen"

#: templates/hyperkitty/overview.html:91
msgid "Activity Summary"
msgstr "Aktivitätszusammenfassung"

#: templates/hyperkitty/overview.html:93
msgid "Post volume over the past <strong>30</strong> days."
msgstr "Posting-Umfang der letzten <strong>30</strong> Tage."

#: templates/hyperkitty/overview.html:98
msgid "The following statistics are from"
msgstr "Die folgenden Statistiken sind von"

#: templates/hyperkitty/overview.html:99
msgid "In"
msgstr "In"

#: templates/hyperkitty/overview.html:100
msgid "the past <strong>30</strong> days:"
msgstr "den letzten <strong>30</strong> Tage:"

#: templates/hyperkitty/overview.html:109
msgid "Most active posters"
msgstr "Die aktivsten Poster"

#: templates/hyperkitty/overview.html:118
msgid "Prominent posters"
msgstr "Prominente Poster"

#: templates/hyperkitty/overview.html:133
msgid "kudos"
msgstr "Kudos"

#: templates/hyperkitty/overview.html:152
msgid "Recent"
msgstr "Kürzliche"

#: templates/hyperkitty/overview.html:156
msgid "Most Active"
msgstr "Aktivste"

#: templates/hyperkitty/overview.html:160
msgid "Most Popular"
msgstr "Beliebteste"

#: templates/hyperkitty/overview.html:166
#: templates/hyperkitty/user_profile/base.html:22
msgid "Favorites"
msgstr "Favoriten"

#: templates/hyperkitty/overview.html:170
msgid "Posted"
msgstr ""

#: templates/hyperkitty/overview.html:179
msgid "Recently active discussions"
msgstr "Zuletzt aktive Diskussionen"

#: templates/hyperkitty/overview.html:186
msgid "Most popular discussions"
msgstr "Beliebteste Diskussionen"

#: templates/hyperkitty/overview.html:193
msgid "Most active discussions"
msgstr "Aktivste Diskussionen"

#: templates/hyperkitty/overview.html:200
msgid "Discussions You've Flagged"
msgstr "Diskussionen, die Sie markiert haben"

#: templates/hyperkitty/overview.html:208
msgid "Discussions You've Posted to"
msgstr "Diskussionen, zu denen Sie beigetragen haben"

#: templates/hyperkitty/reattach.html:9
msgid "Reattach a thread"
msgstr "Diskussionsstrang wieder anhängen"

#: templates/hyperkitty/reattach.html:20
msgid "Re-attach a thread to another"
msgstr "Diskussionsstrang an einen anderen wieder anhängen"

#: templates/hyperkitty/reattach.html:22
msgid "Thread to re-attach:"
msgstr "Diskussionsstrang der wieder angehängt werden soll:"

#: templates/hyperkitty/reattach.html:29
msgid "Re-attach it to:"
msgstr "Wieder anhängen an:"

#: templates/hyperkitty/reattach.html:31
msgid "Search for the parent thread"
msgstr "Den übergeordneten Diskussionsstrang suchen"

#: templates/hyperkitty/reattach.html:32
msgid "Search"
msgstr "Suchen"

#: templates/hyperkitty/reattach.html:44
msgid "this thread ID:"
msgstr "diese Diskussionsstrang-ID:"

#: templates/hyperkitty/reattach.html:50
msgid "Do it"
msgstr "Tu es"

#: templates/hyperkitty/reattach.html:50
msgid "(there's no undoing!), or"
msgstr "(es gibt kein zurück!), oder"

#: templates/hyperkitty/reattach.html:52
msgid "go back to the thread"
msgstr "zurück zum Thread"

#: templates/hyperkitty/search_results.html:8
msgid "Search results for"
msgstr "Suche Ergebnisse nach"

#: templates/hyperkitty/search_results.html:30
msgid "search results"
msgstr "Suchergebnisse"

#: templates/hyperkitty/search_results.html:32
msgid "Search results"
msgstr "Suchergebnisse"

#: templates/hyperkitty/search_results.html:34
msgid "for query"
msgstr "als Abfrage"

#: templates/hyperkitty/search_results.html:44
#: templates/hyperkitty/user_posts.html:34
msgid "messages"
msgstr "Nachrichten"

#: templates/hyperkitty/search_results.html:57
msgid "sort by score"
msgstr "sortieren (nach Punkten)"

#: templates/hyperkitty/search_results.html:60
msgid "sort by latest first"
msgstr "sortieren (neuste zuerst)"

#: templates/hyperkitty/search_results.html:63
msgid "sort by earliest first"
msgstr "sortieren (älteste zuerst)"

#: templates/hyperkitty/search_results.html:84
msgid "Sorry no email could be found for this query."
msgstr ""
"Verzeihung, aber für diese Abfrage konnte keine E-Mail gefunden werden."

#: templates/hyperkitty/search_results.html:87
msgid "Sorry but your query looks empty."
msgstr "Verzeihung, aber die Abfrage sieht leer aus."

#: templates/hyperkitty/search_results.html:88
msgid "these are not the messages you are looking for"
msgstr "das sind nicht die Nachrichten nach den du suchst"

#: templates/hyperkitty/thread.html:30
msgid "newer"
msgstr "neuer"

#: templates/hyperkitty/thread.html:44
msgid "older"
msgstr "älter"

#: templates/hyperkitty/thread.html:72
msgid "First Post"
msgstr "Erstes Posting"

#: templates/hyperkitty/thread.html:75
#: templates/hyperkitty/user_profile/favorites.html:45
#: templates/hyperkitty/user_profile/last_views.html:50
msgid "Replies"
msgstr "Antworten"

#: templates/hyperkitty/thread.html:97
msgid "Show replies by thread"
msgstr "Zeige Antworten nach Diskussionsstrang"

#: templates/hyperkitty/thread.html:100
msgid "Show replies by date"
msgstr "Zeige Antworten nach Datum"

#: templates/hyperkitty/thread.html:113
msgid "Visit here for a non-javascript version of this page."
msgstr ""

#: templates/hyperkitty/thread_list.html:70
msgid "Sorry no email threads could be found"
msgstr "Verzeihung, aber es konnten keine Diskussionsstränge gefunden werden"

#: templates/hyperkitty/threads/category.html:7
msgid "Click to edit"
msgstr "Zum Bearbeiten anklicken"

#: templates/hyperkitty/threads/category.html:9
msgid "You must be logged-in to edit."
msgstr "Sie müssen zum Bearbeiten angemeldet sein."

#: templates/hyperkitty/threads/category.html:15
msgid "no category"
msgstr "keine Kategorie"

#: templates/hyperkitty/threads/right_col.html:12
msgid "Age (days ago)"
msgstr "Alter (vor Tagen)"

#: templates/hyperkitty/threads/right_col.html:18
msgid "Last active (days ago)"
msgstr "Zuletzt aktiv (vor Tagen)"

#: templates/hyperkitty/threads/right_col.html:40
#, python-format
msgid "%(num_comments)s comments"
msgstr "%(num_comments)s Kommentare"

#: templates/hyperkitty/threads/right_col.html:44
#, python-format
msgid "%(participants_count)s participants"
msgstr "%(participants_count)s Teilnehmer"

#: templates/hyperkitty/threads/right_col.html:49
#, python-format
msgid "%(unread_count)s unread <span class=\"hidden-sm\">messages</span>"
msgstr ""
"%(unread_count)s ungelesene <span class=\"hidden-sm\">Nachrichten</span>"

#: templates/hyperkitty/threads/right_col.html:59
msgid "You must be logged-in to have favorites."
msgstr "Sie müssen angemeldet sein, um Favoriten zu haben."

#: templates/hyperkitty/threads/right_col.html:60
msgid "Add to favorites"
msgstr "Zu Favoriten hinzufügen"

#: templates/hyperkitty/threads/right_col.html:62
msgid "Remove from favorites"
msgstr "Von Favoriten entfernen"

#: templates/hyperkitty/threads/right_col.html:71
msgid "Reattach this thread"
msgstr "Diesen Diskussionsstrang wieder anhängen"

#: templates/hyperkitty/threads/right_col.html:75
msgid "Delete this thread"
msgstr "Diesen Diskussionsstrang löschen"

#: templates/hyperkitty/threads/right_col.html:113
msgid "Unreads:"
msgstr "Ungelesene:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "Go to:"
msgstr "Gehe zu:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "next"
msgstr "weitere"

#: templates/hyperkitty/threads/right_col.html:116
msgid "prev"
msgstr "vorherige"

#: templates/hyperkitty/threads/summary_thread_large.html:21
#: templates/hyperkitty/threads/summary_thread_large.html:23
msgid "Favorite"
msgstr "Favorit"

#: templates/hyperkitty/threads/summary_thread_large.html:38
msgid "Most recent thread activity"
msgstr "Letzte Aktivität im Diskussionsstrang"

#: templates/hyperkitty/threads/summary_thread_large.html:59
msgid "comments"
msgstr "Kommentare"

#: templates/hyperkitty/threads/tags.html:3
msgid "tags"
msgstr "Stichworte"

#: templates/hyperkitty/threads/tags.html:9
msgid "Search for tag"
msgstr "Nach Stichwort suchen"

#: templates/hyperkitty/threads/tags.html:15
msgid "Remove"
msgstr "Entfernen"

#: templates/hyperkitty/user_posts.html:8
#: templates/hyperkitty/user_posts.html:21
#: templates/hyperkitty/user_posts.html:25
msgid "Messages by"
msgstr "Nachrichten von"

#: templates/hyperkitty/user_posts.html:38
#, python-format
msgid "Back to %(fullname)s's profile"
msgstr "Zurück zum Profil von %(fullname)s"

#: templates/hyperkitty/user_posts.html:48
msgid "Sorry no email could be found by this user."
msgstr ""
"Verzeihung, es konnte keine E-Mail von diesem Benutzer gefunden werden."

#: templates/hyperkitty/user_profile/base.html:5
#: templates/hyperkitty/user_profile/base.html:12
msgid "User posting activity"
msgstr "Benutzer Posting Aktivität"

#: templates/hyperkitty/user_profile/base.html:12
#: templates/hyperkitty/user_public_profile.html:7
#: templates/hyperkitty/user_public_profile.html:14
msgid "for"
msgstr "für"

#: templates/hyperkitty/user_profile/base.html:26
msgid "Threads you have read"
msgstr "Diskussionsstränge, die Sie gelesen haben"

#: templates/hyperkitty/user_profile/base.html:30
#: templates/hyperkitty/user_profile/profile.html:18
#: templates/hyperkitty/user_profile/subscriptions.html:45
msgid "Votes"
msgstr "Stimmen"

#: templates/hyperkitty/user_profile/base.html:34
msgid "Subscriptions"
msgstr "Abonnements"

#: templates/hyperkitty/user_profile/favorites.html:24
#: templates/hyperkitty/user_profile/last_views.html:27
#: templates/hyperkitty/user_profile/votes.html:23
msgid "Original author:"
msgstr "Ursprünglicher Verfasser:"

#: templates/hyperkitty/user_profile/favorites.html:26
#: templates/hyperkitty/user_profile/last_views.html:29
#: templates/hyperkitty/user_profile/votes.html:25
msgid "Started on:"
msgstr "Gestartet am:"

#: templates/hyperkitty/user_profile/favorites.html:28
#: templates/hyperkitty/user_profile/last_views.html:31
msgid "Last activity:"
msgstr "Letzte Aktivität:"

#: templates/hyperkitty/user_profile/favorites.html:30
#: templates/hyperkitty/user_profile/last_views.html:33
msgid "Replies:"
msgstr "Antworten:"

#: templates/hyperkitty/user_profile/favorites.html:41
#: templates/hyperkitty/user_profile/last_views.html:46
#: templates/hyperkitty/user_profile/profile.html:16
#: templates/hyperkitty/user_profile/votes.html:47
msgid "Subject"
msgstr "Betreff"

#: templates/hyperkitty/user_profile/favorites.html:42
#: templates/hyperkitty/user_profile/last_views.html:47
#: templates/hyperkitty/user_profile/votes.html:48
msgid "Original author"
msgstr "Ursprünglicher Verfasser"

#: templates/hyperkitty/user_profile/favorites.html:43
#: templates/hyperkitty/user_profile/last_views.html:48
#: templates/hyperkitty/user_profile/votes.html:49
msgid "Start date"
msgstr "Startdatum"

#: templates/hyperkitty/user_profile/favorites.html:44
#: templates/hyperkitty/user_profile/last_views.html:49
msgid "Last activity"
msgstr "Letzte Aktivität"

#: templates/hyperkitty/user_profile/favorites.html:71
msgid "No favorites yet."
msgstr "Noch keine Favoriten."

#: templates/hyperkitty/user_profile/last_views.html:22
#: templates/hyperkitty/user_profile/last_views.html:59
msgid "New comments"
msgstr "Neue Kommentare"

#: templates/hyperkitty/user_profile/last_views.html:82
msgid "Nothing read yet."
msgstr "Noch nichts gelesen."

#: templates/hyperkitty/user_profile/profile.html:9
msgid "Last posts"
msgstr "Letzte Postings"

#: templates/hyperkitty/user_profile/profile.html:17
msgid "Date"
msgstr "Datum"

#: templates/hyperkitty/user_profile/profile.html:19
msgid "Thread"
msgstr "Diskussionsstrang"

#: templates/hyperkitty/user_profile/profile.html:20
msgid "Last thread activity"
msgstr "Letzte Aktivität im Diskussionsstrang"

#: templates/hyperkitty/user_profile/profile.html:49
msgid "No posts yet."
msgstr "Noch keine Postings."

#: templates/hyperkitty/user_profile/subscriptions.html:24
msgid "since first post"
msgstr "seit erstem Posting"

#: templates/hyperkitty/user_profile/subscriptions.html:26
#: templates/hyperkitty/user_profile/subscriptions.html:63
msgid "post"
msgstr "Posting"

#: templates/hyperkitty/user_profile/subscriptions.html:31
#: templates/hyperkitty/user_profile/subscriptions.html:69
msgid "no post yet"
msgstr "noch kein Posting"

#: templates/hyperkitty/user_profile/subscriptions.html:42
msgid "Time since the first activity"
msgstr "Zeit seit der ersten Aktivität"

#: templates/hyperkitty/user_profile/subscriptions.html:43
msgid "First post"
msgstr "Erstes Posting"

#: templates/hyperkitty/user_profile/subscriptions.html:44
msgid "Posts to this list"
msgstr "Postings an diese Liste"

#: templates/hyperkitty/user_profile/subscriptions.html:76
msgid "no subscriptions"
msgstr "keine Abonnements"

#: templates/hyperkitty/user_profile/votes.html:32
#: templates/hyperkitty/user_profile/votes.html:70
msgid "You like it"
msgstr "Ihnen gefällt es"

#: templates/hyperkitty/user_profile/votes.html:34
#: templates/hyperkitty/user_profile/votes.html:72
msgid "You dislike it"
msgstr "Ihnen gefällt es nicht"

#: templates/hyperkitty/user_profile/votes.html:50
msgid "Vote"
msgstr "Abstimmen"

#: templates/hyperkitty/user_profile/votes.html:83
msgid "No vote yet."
msgstr "Noch keine Stimmen."

#: templates/hyperkitty/user_public_profile.html:7
msgid "User Profile"
msgstr "Benutzerprofil"

#: templates/hyperkitty/user_public_profile.html:14
msgid "User profile"
msgstr "Benutzerprofil"

#: templates/hyperkitty/user_public_profile.html:23
msgid "Name:"
msgstr "Name:"

#: templates/hyperkitty/user_public_profile.html:28
msgid "Creation:"
msgstr "Erstellt am:"

#: templates/hyperkitty/user_public_profile.html:33
msgid "Votes for this user:"
msgstr "Stimmen für diesen Benutzer:"

#: templates/hyperkitty/user_public_profile.html:41
msgid "Email addresses:"
msgstr "E-Mail Adressen:"

#: views/message.py:75
msgid "This message in gzipped mbox format"
msgstr "Diese Nachricht in komprimierten (gzip) mbox-Format"

#: views/message.py:201
msgid "Your reply has been sent and is being processed."
msgstr "Ihre Antwort wurde gesendet und wird bearbeitet."

#: views/message.py:205
msgid ""
"\n"
"  You have been subscribed to {} list."
msgstr ""
"\n"
"  Sie haben die Liste {} abonniert."

#: views/message.py:288
#, python-format
msgid "Could not delete message %(msg_id_hash)s: %(error)s"
msgstr "Konnte Nachricht nicht löschen: %(msg_id_hash)s: %(error)s"

#: views/message.py:297
#, python-format
msgid "Successfully deleted %(count)s messages."
msgstr "Erfolgreich %(count)s Nachrichten gelöscht."

#: views/mlist.py:88
msgid "for this MailingList"
msgstr "für diese MailingListe"

#: views/mlist.py:100
msgid "for this month"
msgstr "Für diesen Monat"

#: views/mlist.py:103
msgid "for this day"
msgstr "Für diesen Tag"

#: views/mlist.py:115
msgid "This month in gzipped mbox format"
msgstr "Dieser Monat in komprimiertem (gzip) mbox-Format"

#: views/mlist.py:250 views/mlist.py:274
msgid "No discussions this month (yet)."
msgstr "(Noch) keine Diskussionen in diesem Monat."

#: views/mlist.py:262
msgid "No vote has been cast this month (yet)."
msgstr "Es wurden in diesem Monat (noch) keine Stimmen abgegeben."

#: views/mlist.py:291
msgid "You have not flagged any discussions (yet)."
msgstr "Sie haben (noch) keine Diskussionen markiert."

#: views/mlist.py:314
msgid "You have not posted to this list (yet)."
msgstr "Sie haben (noch) nichts an diese Liste gesendet."

#: views/mlist.py:407
msgid "You must be a staff member to delete a MailingList"
msgstr "Sie benötigen Administratoren-Rechte zum Löschen einer Liste"

#: views/mlist.py:421
msgid "Successfully deleted {}"
msgstr "{} erfolgreich gelöscht"

#: views/search.py:115
#, python-format
msgid "Parsing error: %(error)s"
msgstr "Parsing Fehler: %(error)s"

#: views/thread.py:192
msgid "This thread in gzipped mbox format"
msgstr "Diesen Diskussionsstrang im komprimierten (gzip) mbox-Format"

#~ msgid "days inactive"
#~ msgstr "Tage inaktiv"

#~ msgid "days old"
#~ msgstr "Tage alt"

#~ msgid ""
#~ "\n"
#~ "                    by %(name)s\n"
#~ "                    "
#~ msgstr ""
#~ "\n"
#~ "                    von %(name)s\n"
#~ "                    "

#~ msgid "unread"
#~ msgstr "nicht gelesen"

#~ msgid "Go to"
#~ msgstr "Gehe zu"

#~ msgid "More..."
#~ msgstr "Mehr..."

#~ msgid "Discussions"
#~ msgstr "Diskussionen"

#~ msgid "most recent"
#~ msgstr "neuste"

#~ msgid "most popular"
#~ msgstr "beliebteste"

#~ msgid "most active"
#~ msgstr "aktivste"

#~ msgid "Update"
#~ msgstr "Aktualisieren"

#~ msgid ""
#~ "\n"
#~ "                                        by %(name)s\n"
#~ "                                    "
#~ msgstr ""
#~ "\n"
#~ "                                        von %(name)s\n"
#~ "                                    "
